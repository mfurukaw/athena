#!/bin/bash
#
# art-description: Simulation test with R4 MS geometry + ITK
# art-type: grid
# art-architecture:  '#x86_64-intel'
# art-athena-mt: 8
# art-output: log.*
# art-output: MuonSimHitNtuple.root
# art-output: SimHitsR4.pool.root


geo_db="/cvmfs/atlas-nightlies.cern.ch/repo/data/data-art/MuonRecRTT/ATLAS-R4-MUONTEST.db"
geo_tag="ATLAS-P2-RUN4-03-00-00"
validNTuple="MuonSimHitNtuple.root"

mkdir -p Geometry/
ln -s ${geo_db} Geometry/${geo_tag}.db

export ATHENA_PROC_NUMBER=8
export ATHENA_CORE_NUMBER=8
Sim_tf.py \
      --CA True \
      --multithreaded True \
      --geometrySQLite True \
      --conditionsTag 'default:OFLCOND-MC21-SDR-RUN4-02' \
      --simulator 'FullG4MT_QS' \
      --postInclude 'PyJobTransforms.TransformUtils.UseFrontier' \
      --preExec "all:from AtlasGeoModel import CommonGeoDB;CommonGeoDB.SetupLocalSqliteGeometryDb(\"Geometry/${geo_tag}.db\",flags.GeoModel.AtlasVersion);" \
      --postExec "all:flags.dump(evaluate=True);from MuonPRDTestR4.MuonHitTestConfig import MuonHitTesterCfg;cfg.merge(MuonHitTesterCfg(flags,dumpSimHits=True, outFile=\"${validNTuple}\"));" \
      --geometryVersion "default:${geo_tag}" \
      --inputEVNTFile '/cvmfs/atlas-nightlies.cern.ch/repo/data/data-art/CampaignInputs/mc21/EVNT/mc21_13p6TeV.601229.PhPy8EG_A14_ttbar_hdamp258p75_SingleLep.evgen.EVNT.e8453/EVNT.29328277._003902.pool.root.1' \
      --outputHITSFile 'SimHitsR4.pool.root' \
      --maxEvents 100 \
      --skipEvents 0 \
      --randomSeed 10 \
      --imf False

rc=$?
echo  "art-result: $rc simulation"

exit ${rc}
