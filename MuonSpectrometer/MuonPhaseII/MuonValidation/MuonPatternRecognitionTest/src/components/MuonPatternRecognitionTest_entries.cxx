/*
  Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
*/

#include "../MuonHoughTransformTester.h"
#include "../MuonRecoChainTester.h"

DECLARE_COMPONENT(MuonValR4::MuonHoughTransformTester)
DECLARE_COMPONENT(MuonValR4::MuonRecoChainTester)
